#include <Servo.h> 
Servo myservo;

int Ana0 = 0;
float Ana1 = 0;

boolean autoPinMode = false;
boolean directMode = false;
int involtDigital[14] = {};
String receivedString[] = {};
String fname;

void setup() {
  //Bitrate must remain same as in app.
  //Serial.begin(9600);
  Serial.begin(19200);

  myservo.attach(9);
  myservo.write(90);

  pinMode(5, OUTPUT);

}


void loop() {
  //receive data from your app, do not remove this line.
  involtReceive();

  Ana0 = analogRead(A0);
  Ana1 = analogRead(A1);

  Ana0 = map(Ana0, 0, 1023, 0, 360);
  Ana1 = map(Ana1, 0, 1020, 0, 500);

  involtSend(0, Ana0);
  involtSend(1, Ana1);
  
  Serial.println(Ana0);
  Serial.println(Ana1);

  analogWrite(5, involtDigital[5] );
  
  delay(20);
  
  //clear the fname to prevent from duplicating functions
  fname = "";
}





//----------------------
//INVOLT FUNCTIONS

String V = "V";
String A = "A";
String E = "E";

void involtSend(int pinNumber, int sendValue) {
  Serial.println(A + pinNumber + V + sendValue + E);
}

void involtSendString(int pinNumber, String sendString) {
  Serial.println(A + pinNumber + V + sendString + E);
}

void involtReceive() {

  String involt;
  int involtLen;
  int pin;
  int val;
  
  String pwm = "P";
  String dig = "D";
  String fn = "FN";
  
  if (Serial.available() > 0) {
    involt = Serial.readStringUntil('\n');
    involtLen = involt.length();

    if (involt.indexOf(fn) == 0) {
      fname = involt.substring(2, involtLen);
    }
    else if (involt.indexOf(dig) == 0 || involt.indexOf(pwm) == 0 ){
      pin = involt.substring(1, involt.indexOf(V)).toInt();
      val = involt.substring(involt.indexOf(V) + 1, involtLen).toInt();
      

      if (autoPinMode) {
        pinMode(pin, OUTPUT);
      };

      if (directMode) {
        if (involt.indexOf(dig) == 0) {
          digitalWrite(pin, val);
        }
        else if (involt.indexOf(pwm) == 0 ) {
          analogWrite(pin, val);
        };
      }
      else {
        involtDigital[pin] = val;
      };

    }
    else if(involt.indexOf("S") == 0){
       pin = involt.substring(1, involt.indexOf(V)).toInt();
       receivedString[pin] = involt.substring(involt.indexOf(V) + 1, involtLen);
    };
    
  };
};
